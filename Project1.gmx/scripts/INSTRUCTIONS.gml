
/* CREATED BY: CLAVOS STUDIOS / FRANCIS CLASE GENAO  ([PREMIUM version Does not require credits])

_______________________________________________________________________________________________________________________________

                                                        |INSTRUCTIONS |
_________________________________________________________________________________________________________________________________

                                        
                                It is very easy to use! (Read all instructions please) :)

                                

>> NAV IN ROOM

        Press (A) to switch to the previous room
        Press (D) to change to the next room
        Press (vk_space) to activate and deactivate the movement of volume system
        And to move the volume system using the keys: (VK_LEFT, VK_RIGHT, VK_UP and VK_DOWN)')
        

>> ADDING SOUND TO EACH volume bar?

        On objects "oVolume _..." in the CREATE event, the object creates multiple ds_lists, 
        which contain lists of sounds to be controlled in the list (each list represents a volume bar)
        This means that each volume bar is guided from a list of sounds (ds_list)
        
        The "ds_list_add ()" function is called in the CREATE event of objects "oVolume _..." is used to add sounds to a "ds_list"
        
        
        FOR EXAMPLE:
        
        //LIST of music
        list_music = ds_list_create();
        ds_list_add list_music, sound1, sound2, sound3);
        
        
        * In the above code sounds are added: sound1, sound2 and Sound3 to the volume bar that controls music
        (The "Ds_list_add ()" function is only compatible with 16 arguments, so you should call back (the number of times you need)
        If you need to add more than 16 sounds to a list)


>> HIDE SYSTEM VOLUME

        Each object "oVolume _..." contains a variable called "hide" when equals false: the system volume is visible and normal, but
        when it equals true system volume becomes invisible and disables all checks collision, leaving active only the processes needed to control the 
        volume (in a kind of background). To test this function has it added a button object "oButton_hide" which when pressed causes change the value of the 
        variable "hide" in any of the objects "oVolume _..." so you can test what is * ATTENTION ! object "oButton_hide" is for you to try, you can remove if 
        you wish, all it does is change the value of the variable "hide" inside objects "oVolume _..." *
        
        
>> MOVING THE SYSTEM VOLUME
        
        In the step event of objects: "oVolume _..." there are two scripts, one of which is dedicated to the movement, the 
        variable "move" is the one that controls the activation of the movements. Everything is ready for the precionar VK_SPACE is 
        activated or movement of the system volume, which moves with the movement keys (vk_left, vk_right, VK_UP and VK_DOWN) is 
        turned off, you can set the speed of the movements in the variable: "velocity". NOTE: It is recommended to disable the movements (Variable "move") 
        when terine to move the system volume
        
        
        >> SET SIZE VOLUME BARS
        
        In the Create event of objects: "oVolume _..." there are two variables: "size_x" and "size_y"; the first to put the horizontal size of the bars, and 
        the second to put the vertical size of the bars. WARNING! sizes defined in the two variables will be used squared. Example: size_x = 100; "100" is 
        used as "200".
        

>> AUTOMATIC VOLUME

    << Advantages:
    You can select the number of bars you want and are automatically created with their respective lists of audio
     -Only Must specify the number of bars you want and automatically creacrán
    -Choose: Number of bars, size, spacing between each bar, color and appearance sprites
    -The Audio playlists are created automatically, you just put the sounds you want to control each bar (More details in the 'instructions' script)
    Maybe add tags to each volume bar, Example: 'Sounds', 'Hello xD', 'Music', 'Effects', etc.


    
        The automatic volume (subject: "oVolume_Automatic") has the other objects of volume systems parecisdas some things.
        
        In the automatic volume can choose the number of bars you want in volume variable "size_volume" (Event CREATE).
        Example: size_volume = 10; (10 volume bars will be created)
        
        As in other systems of volume, choose the size of the volume bars with two variables: "size_x" and "size_y" (Event CREATE).
        
        Choose the vertical spacing between each of the bars with the variable volume "separate" (Event CREATE).
        
        Sounds lists are added to the script: "audio_config ()" with "ds_list_add ()" function as an array, depending on the number of bars that 
        put (must understand the arrays for this), Example: ds_list_add (volume [0], sound01); ds_list_add (volume [1], sound02) ;. 
        The "Text Labels" are added arrays, Example: label [0] = 'Example'; label [1] = 'Example'; ATTENTION !: Check the "audio_config ()" script to 
        see the operation.
        
        *In the CREATE event is a variable named "color_bars [d]" (It is an array), which is in a "for", stood as array so you can set a different color for each
         volume bar if desired , you have to do to achieve this is to make the array of "for" and assign different color values ​​for each position of the array.
         

>> The music that accompanies this asset can not be used for any purpose without contacting your COMPOSER: Paltian / WEB: www.paltian.com
