///generate_chunk_grid()
/*
    Creates a grid of "chunks" which make up the level.
    Should be followed by the chunks_to_level script to
    convert those "chunks" in to a grid of blocks in the
    level.
    
    The chunks created always guaratee a route between
    the top of the level to the bottom.
*/
var xx, yy;
var w = objLevel.level_width;
var h = objLevel.level_height;
ds_grid_clear(objLevel.chunk_grid, sprChunkStraight);
var drop_list = ds_list_create(); //< list of chunks which drop down to the next layer
for (yy = 0; yy < h; yy++)
{
    var first_layer = (yy == 0); //is this the first layer of the level?
    var last_layer = (yy == h-1); //is this the last layer of the level?
    if (!last_layer)
    {
        ds_list_clear(drop_list);
        repeat(set_chance(20, 2, 1)) //20% chance of 2 openings, otherwise 1
        {
            xx = floor(random(w)); //get a random chunk on the layer
            var chunk_sprite = objLevel.chunk_grid[# xx, yy];
            //change the sprite of the found sprite to one with an
            //opening at the bottom
            switch (chunk_sprite)
            {
                case sprChunkStraight:
                    objLevel.chunk_grid[# xx, yy] = sprChunkOpenBottom;
                    objLevel.chunk_grid[# xx, yy+1] = sprChunkOpenTop;
                    break;
                case sprChunkOpenTop:
                    objLevel.chunk_grid[# xx, yy] = sprChunkOpen;
                    objLevel.chunk_grid[# xx, yy+1] = sprChunkOpenTop;
                    break;
            }
            //add this chunk to the list of chunks which drop down
            ds_list_add(drop_list, xx);
        }
    }
    if !(first_layer && last_layer) //don't encroach if this is the only layer
    {
        //the offset variable is the number of cells each side of the
        //starting cell which will definitely be open, the remaining cells
        //have a percent chance of being open or blocked
        var percent = 80;
        var offset = choose(0, 1);
        //we want the first & last layers of the level to have a higher
        //offset, but a 100% blocked cell chance
        if (first_layer || last_layer)
        {
            percent = 100;
            offset = choose(1, 2);
        }
        //Encroach the left-hand-side
        var x_left = ds_list_find_min(drop_list);
        generation_encroach(0, x_left-offset, yy, percent);
        //Encroach the right-hand-side
        var x_right = ds_list_find_max(drop_list);
        generation_encroach(w-1, x_right+offset, yy, percent);
    }
}
ds_list_destroy(drop_list);


///Additional chunks!
///After the initial chunks have been created,
///you may wish to add some additional chunks, right?
///Here we have start & end chunks as well as some
///special chunks which contain items & stuff.

///Starting chunks
repeat(round_chance(global.chunks_start))
{
    do
    {
        xx = floor(random(w));
        yy = 0;
    }
    until (objLevel.chunk_grid[# xx, yy] == sprChunkStraight);
    objLevel.chunk_grid[# xx, yy] = sprChunkStart;
}
///Ending chunks
repeat(round_chance(global.chunks_start))
{
    do
    {
        xx = floor(random(w));
        yy = h-1;
    }
    until (objLevel.chunk_grid[# xx, yy] == sprChunkStraight);
    objLevel.chunk_grid[# xx, yy] = sprChunkEnd;
}
///Special chunks
repeat(round_chance(global.chunks_special))
{
    var i = 0; //number of attempts to find space
    do
    {
        xx = floor(random(w));
        yy = floor(random(h));
        i++;
    }
    until (objLevel.chunk_grid[# xx, yy] == sprChunkStraight || i > 50);
    if (i > 50) break; //break the loop if no space found
    objLevel.chunk_grid[# xx, yy] = sprChunkSpecial;
}
