///block_empty(block)
/*
 * Is this block empty?!
 * Enter all non-collidable blocks below
 * noone is no block.
 */
var b = argument0;
return (b == noone);
