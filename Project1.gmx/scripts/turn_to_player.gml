/// Turn boss to player if it exists

target = instance_nearest(x, y, objPlayer);

if (target) {
    dir = ((target.x > x)*2)-1;
    facing = dir;
}

