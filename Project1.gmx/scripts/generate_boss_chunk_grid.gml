///generate_boss_chunk_grid()
switch (global.current_room.boss)
    {
        case objSlimeBoss:
            global.current_room.chunk_grid[# 0, 0] = sprSBChunkStart;
            global.current_room.chunk_grid[# 1, 0] = sprSBChunkStraight;
            global.current_room.chunk_grid[# 2, 0] = sprSBChunkBoss;
            global.current_room.chunk_grid[# 3, 0] = sprSBChunkStraight;
            global.current_room.chunk_grid[# 4, 0] = sprSBChunkEnd;
            break;
            
        case objHenryBoss:
            global.current_room.chunk_grid[# 0, 0] = sprHBChunkStart;
            global.current_room.chunk_grid[# 1, 0] = sprHBChunkEnd;
            break;
            
        case objEthanBoss:
            global.current_room.chunk_grid[# 0, 0] = sprEBChunkStart;
            global.current_room.chunk_grid[# 1, 0] = sprEBChunkEnd;
            break;
            
        case objDanielBoss:
            global.current_room.chunk_grid[# 0, 0] = sprDBChunkStart;
            global.current_room.chunk_grid[# 1, 0] = sprDBChunkEnd;
            break;
    }
    
