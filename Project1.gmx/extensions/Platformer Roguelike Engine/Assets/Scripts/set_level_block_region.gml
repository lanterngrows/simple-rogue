///set_level_block_region(x1, y1, x2, y2, sprite)
var xx, yy, x1, x2, y1, y2, spr;
x1 = argument0;
y1 = argument1;
x2 = argument2;
y2 = argument3;
spr = argument4;

if (x1 == x2)
{
    xx = x1;
    for (yy = y1; yy < y2; yy++)
    {
        set_level_block(xx, yy, spr);
    }
}
else
if (y1 == y2)
{
    yy = y1;
    for (xx = x1; xx < x2; xx++)
    {
        set_level_block(xx, yy, spr);
    }
}
else
for (xx = x1; xx < x2; xx++)
for (yy = y1; yy < y2; yy++)
{
    set_level_block(xx, yy, spr);
}
