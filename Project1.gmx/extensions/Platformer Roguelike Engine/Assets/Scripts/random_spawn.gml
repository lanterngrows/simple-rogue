///random_spawn(object, x_offset, y_offset, block_below, block_above)
/*
 * Returns the object index of the spawned instance, or noone if unsuccessful
 * 
 * x/y_offset is the position relative to the grid
 * block_above is whether there must be a block above the object
 * block_below is whether there must be a block below the object
 */
var obj, xx, yy, below, above, w, h, i, px, py, s;
obj = argument0;
xx = argument1;
yy = argument2;
below = argument3;
above = argument4;
w = objLevel.grid_width-1;
h = objLevel.grid_height-1;
s = global.tile_size;
repeat(20) //20 attempts
{
    //get a random position within the level grid
    i = find_empty_block(0, 0, w, h, below, above);
    //calculate the actual position of the spawn
    px = i[0]*s+xx;
    py = i[1]*s+yy;
    //check if there's no entities on the spawn position
    if (!collision_point(px, py, objEntity, false, true))
    {
        //return the created object if successful
        return instance_create(px, py, obj);
    }
}
//return noone if unsuccessful
return noone;

