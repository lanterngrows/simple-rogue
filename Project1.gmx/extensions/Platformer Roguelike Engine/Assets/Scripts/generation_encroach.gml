///generation_encroach(x_from, x_to, y, encroachment_percent)
/*
    Encroach from the sides of the level towards the "centre"
    and creates blocked chunks where possible.
    
    encroachment_percent - chance of creating a blocked chunk
*/
var x1 = argument0;
var x2 = argument1;
var yy = argument2;
var percent = argument3;
while (x1 != x2)
{
    if (objLevel.chunk_grid[# x1, yy] == sprChunkStraight)
    {
        if (percent_chance(percent))
        {
            objLevel.chunk_grid[# x1, yy] = sprChunkBlocked;
        }
    }
    else
    {
        break;
    }
    x1 = approach(x1, x2, 1);
}




