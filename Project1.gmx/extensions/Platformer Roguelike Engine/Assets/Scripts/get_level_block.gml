///get_level_block(grid_x, grid_y)
/*
 * returns the sprite of the level grid at the given position
 * It's faster to just use objLevel.level_grid[# x, y]
 * so use that if you prefer.
 */
return objLevel.level_grid[# argument0, argument1];
