///generate_boss_level()

//Generate the chunk grid
generate_boss_chunk_grid();
//Now loop through the level creating level chunks
chunks_to_level();
//Set the borders of the level to be wall blocks
create_borders(sprWallBlock);
