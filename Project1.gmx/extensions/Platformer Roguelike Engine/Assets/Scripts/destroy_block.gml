///destroy_block(grid_x, grid_y)
///but only if it's destroyable!
/*
 * Destroy a block, create particles & pieces
 * destroy all objects which are connected to the block
 * like arrows & spikes for example.
 *
 * NOTE:
 * Remember to re-draw that section of the level after
 * destroying a block!
 */
var xx = argument0;
var yy = argument1;
var redraw = false;
var block = get_level_block(xx, yy);

///Check if the block is not already empty
if (!block_empty(block))
{
    ///Check if the given block is destructable
    if (block_breakable(block))
    {
        ///Set grid value to nothing
        set_level_block(xx, yy, noone);
        var s = global.tile_size;
        var border = 3;
        ///Create particle pieces
        repeat(4)
        {
            ///get a random x & y position within the block
            var px = irandom_range(xx*s+border, xx*s+s-border);
            var py = irandom_range(yy*s+border, yy*s+s-border);
            ///create the piece
            var a = instance_create(px, py, objPieceTrail);
            a.image_alpha = random_range(1.5, 4);
            ///Set sprite of particles depending on block value
            switch (block)
            {
                case sprTopDirtBlock:
                case sprDirtBlock:
                case sprDirtBlockFloat:
                case sprTopDirtBlockFloat:
                    a.sprite_index = sprBrownPiece;
                    break;
                case sprTopBricksBlock:
                case sprBricksBlock:
                case sprBricksBlockFloat:
                case sprTopBricksBlockFloat:
                case sprStoneBlock:
                    a.sprite_index = sprGreyPiece;
                    break;
            }
        }
        
        ///Destroy any objects which are "stuck in" this block
        with (objArrowStuck)
        if (block_x == xx && block_y == yy)
        {
            instance_destroy();
        }
        with (objSpikes)
        if (block_x == xx && block_y == yy)
        {
            instance_destroy();
        }
        with (objBat)
        if (block_x == xx && block_y == yy)
        {
            instance_destroy();
        }
        
        ///Redraw the level surface if necessary
        if (redraw)
        {
            draw_level(xx-1, yy-1, xx+1, yy+1);
        }
    }
}
