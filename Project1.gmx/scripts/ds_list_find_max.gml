///ds_list_find_max(id)
/*
 * returns the maximum value in a given list
 */
var list = argument0;
var value = list[|0];
for (var i = 0; i < ds_list_size(list); i++)
{
    value = max(value, list[|i]);
}
return value;
