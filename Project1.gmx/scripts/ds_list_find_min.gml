///ds_list_find_min(id)
/*
 * returns the minimum value in a given list
 */
var list = argument0;
var value = list[|0];
for (var i = 0; i < ds_list_size(list); i++)
{
    value = min(value, list[|i]);
}
return value;
