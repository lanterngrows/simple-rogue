///update_blocks(x1, y1, x2, y2)
/*
 * Update the sprite and image_index of all the blocks
 * within the given x1, y1, x2 & y2 rectangle.
 */
var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var grid = global.current_room.level_grid;

for (var xx = x1; xx < x2; xx++)
for (var yy = y1; yy < y2; yy++)
{
    var spr = get_level_block(xx, yy);
    if (spr != noone)
    {
        var sprite = spr;
        switch (spr)
        {
            case sprDirtBlock:
                if (percent_chance(2))
                {
                    sprite = sprStoneBlock;
                }
                else
                {
                    var above = block_solid(get_level_block(xx, yy-1));
                    var below = block_solid(get_level_block(xx, yy+1));
                    if (!above && !below)
                    {
                        sprite = sprTopDirtBlockFloat;
                    }
                    else
                    if (!above && below)
                    {
                        sprite = sprTopDirtBlock;
                    }
                    else
                    if (above && !below)
                    {
                        sprite = sprDirtBlockFloat;
                    }
                }
                break;
            case sprBricksBlock:
                var above = block_solid(get_level_block(xx, yy-1));
                var below = block_solid(get_level_block(xx, yy+1));
                if (!above && !below)
                {
                    sprite = sprTopBricksBlockFloat;
                }
                else
                if (!above && below)
                {
                    sprite = sprTopBricksBlock;
                }
                else
                if (above && !below)
                {
                    sprite = sprBricksBlockFloat;
                }
                break;
            case sprWallBlock:
                var below = block_solid(get_level_block(xx, yy+1));
                if (!below)
                {
                    sprite = sprWallBlockFloat;
                }
                break;
        }
        var index = irandom(sprite_get_number(sprite));
        global.current_room.level_grid[# xx, yy] = sprite;
        global.current_room.image_grid[# xx, yy] = index;
    }
}
