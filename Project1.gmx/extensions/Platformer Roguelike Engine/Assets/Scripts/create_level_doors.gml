///create_level_doors(entrance_level, exit_level)
/*
    This creates entrance and exit doors within the level
    
    entrance_level - the chunk level to create the entrance door at
    exit_level - the chunk level to create the exit door at
*/
var xx, yy, x1, y1, x2, y2, i;
var w = objLevel.level_width;
var h = objLevel.level_height;
var chunk_w = global.chunk_w;
var chunk_h = global.chunk_h;
var s = global.tile_size;

///Entrance door
///Find a suitable chunk to spawn the entrance door in
yy = argument0;
do
{
    xx = floor(random(w));
}
until (objLevel.chunk_grid[# xx, yy] == sprChunkStart);
///Find a block within that chunk to spawn the door on
x1 = xx*chunk_w+objLevel.border_width;
y1 = yy*chunk_h+objLevel.border_height;
x2 = x1+chunk_w;
y2 = y1+chunk_h;
i = find_empty_block(x1, y1, x2, y2, true, false);
///Create the door!
instance_create(i[0]*s, i[1]*s, objDoorEntrance);

///Exit door
///Find a suitable chunk to spawn the exit door in
yy = argument1;
do
{
    xx = floor(random(w));
}
until (objLevel.chunk_grid[# xx, yy] == sprChunkEnd);
///Find a block within that chunk to spawn the door on
x1 = xx*chunk_w+objLevel.border_width;
y1 = yy*chunk_h+objLevel.border_height;
x2 = x1+chunk_w;
y2 = y1+chunk_h;
i = find_empty_block(x1, y1, x2, y2, true, false);
///Create the door!
instance_create(i[0]*s, i[1]*s, objDoorExit);


