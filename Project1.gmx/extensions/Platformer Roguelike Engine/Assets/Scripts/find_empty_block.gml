///find_empty_block(x1, y1, x2, y2, block_below, block_above)
///returns an array which consists of x & y co-ordinates of the empty block
var xx, yy, i, block_below, block_above;
var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
block_below = argument4;
block_above = argument5;
do
{
    xx = floor(random_range(x1, x2));
    yy = floor(random_range(y1, y2));
}
until
(
     block_empty(get_level_block(xx, yy)) &&
    (!block_below || block_solid(get_level_block(xx, yy+1))) &&
    (!block_above || block_solid(get_level_block(xx, yy-1)))
);

i[0] = xx;
i[1] = yy;
return i;
