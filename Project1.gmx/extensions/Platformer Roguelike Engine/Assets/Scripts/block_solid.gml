///block_solid(block)
/*
 * Is this block solid?!
 * All blocks default to solid unless entered
 * below in this code.
 */
var b = argument0;
return !(b == noone ||
         b == sprPlatform);
        
        
        
