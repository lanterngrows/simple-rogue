///Exit door
///Find a suitable chunk to spawn the exit door in
var xx, yy, x1, y1, x2, y2, i;
var w = global.current_room.level_width;
//var h = global.current_room.level_height;
var chunk_w = global.chunk_w;
var chunk_h = global.chunk_h;
var s = global.tile_size;

yy = argument1;
do
{
    xx = floor(random(w));
}
until (global.current_room.chunk_grid[# xx, yy] == global.current_room.end_chunk);
///Find a block within that chunk to spawn the door on
x1 = xx*chunk_w+global.current_room.border_width;
y1 = yy*chunk_h+global.current_room.border_height;
x2 = x1+chunk_w;
y2 = y1+chunk_h;
i = find_empty_block(x1, y1, x2, y2, true, false);
///Create the door!
instance_create(i[0]*s, i[1]*s, objDoorExit);
