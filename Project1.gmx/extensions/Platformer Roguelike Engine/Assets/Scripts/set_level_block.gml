///set_level_block(grid_x, grid_y, sprite, [index])
var xx = argument0;
var yy = argument1;
objLevel.level_grid[# xx, yy] = argument2;
if (argument_count > 3)
{
    objLevel.image_grid[# xx, yy] = argument[3];
}
