///create_borders(sprite)
/*
    Creates blocks around the level depending on
    current room's border width and height.
    
    sprite - the sprite of the blocks to border the level with
*/
var gw = global.current_room.grid_width-1;
var gh = global.current_room.grid_height-1;
var xo = global.current_room.border_width;
var yo = global.current_room.border_height;
var spr = argument0;
set_level_block_region(0, 0, gw, yo, spr); //top
set_level_block_region(0, 0, xo, gh, spr); //left
set_level_block_region(gw-xo+1, 0, gw, gh, spr); //right
set_level_block_region(0, gh-yo+1, gw, gh, spr); //bottom

///Set the bottom layer to be dirt
set_level_block_region(xo, gh-yo+1, gw-xo+1, gh-yo+1, sprDirtBlock);
