///create_borders(sprite)
/*
    Creates blocks around the level depending on
    objLevel's border width and height.
    
    sprite - the sprite of the blocks to border the level with
*/
var gw = objLevel.grid_width-1;
var gh = objLevel.grid_height-1;
var xo = objLevel.border_width;
var yo = objLevel.border_height;
var spr = argument0;
set_level_block_region(0, 0, gw, yo, spr); //top
set_level_block_region(0, 0, xo, gh, spr); //left
set_level_block_region(gw-xo+1, 0, gw, gh, spr); //right
set_level_block_region(0, gh-yo+1, gw, gh, spr); //bottom

///Set the bottom layer to be dirt
set_level_block_region(xo, gh-yo+1, gw-xo+1, gh-yo+1, sprDirtBlock);
