/*
_________________________________________________________________________

| 2.6.0 | - CURRENT VERSION

  CHANGES:

    NEWS:

     ****|VOLUME SYSTEN FREE DEPRECATED|****
     ****|VOLUME SYSTEN PREMIUM WAS RELEASED FOR FREE|****
     
     
_________________________________________________________________________
     
     
| 2.5.0 |


  CHANGES:

    NEWS:
    
     *NEW SYSTEM OF AUTOMATIC VOLUME added !!, you can choose the number of bars you want and are automatically created with their respective lists of audio
        -Only Must specify the number of bars you want and create automatically
        -Choose: Number of bars, size, spacing between each bar, color and appearance sprites
        -The Audio playlists are created automatically, you just put the sounds you want to control each bar (More details in the 'instructions' script)
        -Maybe add tags to each volume bar, Example: 'Sounds', 'Hello xD', 'Music', 'Effects', etc.
        -Very easy to adapt
     *(APPEARANCE): New Volume Skim round
     *(APPEARANCE): New Skim of Rocket (vertical Volume)
     *Added gradual rotation in Volume Circular
     *Now use the STEP BEGIN event is made for greater control
     *To hide the system volume now native variable is used 'visible'
     *Code shorter and efficient
     *Faster response time
     *All test sounds have been changed and added more (can not use in their games have the copyright of the great composer [Paltian / James Cáseres])
     *The instructions Have Been updated
     *Added Script "release_notes" to record the Changes Between versions
     *Variable 'mode' was changed to 'hide'
     *Now the final position of the system volume is saved and loaded at startup to avoid errors
     *Removed objects buttons 'oButton_Mode' added item button 'oButton_hide' instead (the work of deleted ago)


_________________________________________________________________________
    
    Bugs Fixed
     *Best calculation of distance to draw sprites in Skins: VolumeCar, VolumePacman, VolumeRocket
     *Fixed bug that caused Great to start the executable, the sound rang a few thousandths (Lag) even if it was disabled (OFF) at the volume control
     *Fixed an infinite loop that caused the infinite creation of the file 'volume.vol'
     *Improved algorithm slip buttons (More performance)
     *Improved movement code
     *Fixed error when trying to move the circular volume
     *Small repairs to Draw event and the Step event (More performance)
     *Repaired area of ​​collision with Wood Skim Master Volume switch
     *Fixed: text font not correctly deactivation
     *Now the activation of each button in a much more efficient, convenient and fast way is checked
     *Checks event DRAW now mixed with activation code in the step event for higher performance
     *Less impact on event Step
     *Change collision_point by collision_circle, and increased the area collision of ​​the Volume circular
______________________________________________________________________
