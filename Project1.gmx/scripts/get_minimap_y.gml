///get_minimap_y(value)
var border_y = global.current_room.border_height*global.tile_size*2;
var yy = argument0;
var map_y = (yy/(global.current_room.pixel_height-border_y)*objMiniMap.map_height-objMiniMap.chunk_height);
return map_y;
