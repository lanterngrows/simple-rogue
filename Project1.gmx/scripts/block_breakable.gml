///block_breakable(block)
/*
 * Is this block breakable?!
 * All blocks default to breakable unless entered
 * below in this code.
 */
var b = argument0;
return !(b == sprWallBlock ||
         b == sprWallBlockFloat);
        
        
        
