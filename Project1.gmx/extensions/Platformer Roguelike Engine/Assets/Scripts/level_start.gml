///level_start(level_number)
/*
 * Start a level!
 * Destroy objLevel, set a new level number and
 * go to room0
 */
with (objLevel)
{
    /*if (surface_exists(level_surface))
    {
        surface_set_target(objLevel.level_surface);
            draw_clear_alpha(0, 0);
        surface_reset_target();
    }*/
    instance_destroy();
}
global.level = argument0;
room_goto(roomLevel);
