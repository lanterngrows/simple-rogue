///level_end()
/*
 * End the level and return to the menu
 * Destroy objLevel, reset the level number and
 * go to roomMenu
 */
/*if (surface_exists(objLevel.level_surface))
{
    surface_set_target(objLevel.level_surface);
        draw_clear_alpha(0, 0);
    surface_reset_target();
}*/
with (objLevel) instance_destroy();
global.level = 1;
room_goto(roomMenu);
