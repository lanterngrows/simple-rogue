///define_colours()
global.v_block = c_black;
global.v_bricks = c_yellow;
global.v_item = c_lime;
global.v_platform = c_blue;
global.v_slime = c_green;
global.v_boss = c_red;

global.v_entrance_door = make_color_rgb(0, 255, 236);
global.v_exit_door = make_color_rgb(128, 128, 128);

//global.v_shop_entrance_door = make_color_rgb(0, 204, 56);
//global.v_shop_exit_door = make_color_rgb(0, 204, 56);
global.v_shop_item_1 = make_color_rgb(34, 76, 255);

