///get_minimap_x(value)
var border_x = objLevel.border_width*global.tile_size*2;
var xx = argument0;
var map_x = (xx/(objLevel.pixel_width-border_x)*objMiniMap.map_width-objMiniMap.chunk_width);
return map_x;
