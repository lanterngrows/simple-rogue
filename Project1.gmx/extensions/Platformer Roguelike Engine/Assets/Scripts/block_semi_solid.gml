///block_semi_solid(block)
/*
 * Is this block semi-solid?!
 * Enter blocks which can be fallen through
 */
var b = argument0;
return (b == sprPlatform);
