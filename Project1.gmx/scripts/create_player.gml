///create_player()
/*
    Creates a player at the entrance door
    OR if a player already exists, move the player
    to the entrance door's position
*/
with (objDoorEntrance) {
    if (instance_exists(objPlayer)) {
        var a = objPlayer;
    } else {
        var a = instance_create(0, 0, objPlayer1);
    }
    
    a.x = x+global.tile_size/2;
    a.y = y+global.tile_size;
    
    with (objCamera) {
        target = a;
        view_xview = target.x-view_wview/2;
        view_yview = target.y-view_hview;
        
        if (global.current_room_alias == BOSS_LEVEL) {
            y_view_koeff = 1.35;
        } else {
            y_view_koeff = 2;
        }
    }
    
    with (objHUD) {
        target = a;
    }
}

