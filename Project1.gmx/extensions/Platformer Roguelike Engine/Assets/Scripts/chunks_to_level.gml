///chunks_to_level()
/*
    Loops though objLevel's chunk_grid and creates
    the level grid and objects from it.
*/
var w = objLevel.level_width;
var h = objLevel.level_height;
var filename = "data.ini";
var chunk_w = global.chunk_w;
var chunk_h = global.chunk_h;
var xo = objLevel.border_width;
var yo = objLevel.border_height;
var s = global.tile_size;
///Clear the grid - get rid of any previous generation stuff
ds_grid_clear(objLevel.level_grid, noone);
for (var yy = 0; yy < h; yy++)
for (var xx = 0; xx < w; xx++)
{
    var chunk_sprite = objLevel.chunk_grid[# xx, yy];
    ///Find a chunk to create in the level
    var section = sprite_get_name(chunk_sprite);
    ini_open(filename);
        var total = ini_read_real(section, "total", 0);
        var i = floor(random(total));
        var str = ini_read_string(section, string(i), "");
        var chunk = ds_grid_create(chunk_w, chunk_h);
        ds_grid_read(chunk, str);
    ini_close();
    
    ///Now that we've selected a grid from our ini file,
    ///let's loop though each cell in the grid and create
    ///the required blocks & objects.
    for (var cell_x = 0; cell_x < chunk_w; cell_x++)
    for (var cell_y = 0; cell_y < chunk_h; cell_y++)
    {
        var cell = chunk[# cell_x, cell_y];
        var actual_x = (xx*chunk_w+cell_x)+xo;
        var actual_y = (yy*chunk_h+cell_y)+yo;
        switch (cell)
        {
            case global.v_block:
                set_level_block(actual_x, actual_y, sprDirtBlock);
                break;
            case global.v_bricks:
                set_level_block(actual_x, actual_y, sprBricksBlock);
                break;
            case global.v_item:
                var obj = choose(objExtraHeart, objHealthPotion);
                instance_create(actual_x*s+s/2, actual_y*s+s/2, obj);
                break;
            case global.v_platform:
                set_level_block(actual_x, actual_y, sprPlatform);
                break;
            case global.v_slime:
                instance_create(actual_x*s+s/2, actual_y*s+s/2, objSlime);
                break;
        }
    }
    
    ///Let's destroy that grid, we're done with it!
    ds_grid_destroy(chunk);
}
