///set_speed(speed, direction)
/*
 * works the same as GM's motion_set function
 * except we're using vx and vy instead of hspeed and vspeed
 */
speed = argument0;
direction = argument1;
vx = hspeed;
vy = vspeed;
hspeed = 0;
vspeed = 0;
vx = max(-vx_max, min(vx_max, vx));
vy = max(-vy_max, min(vy_max, vy));
