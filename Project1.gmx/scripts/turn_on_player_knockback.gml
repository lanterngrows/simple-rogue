///turn_on_player_knockback
with objPlayer1 {    
    with melee_weapon {
        knockback = default_knockback;
    }
    
    with ranged_weapon {
        knockback = default_knockback;
    }
}

