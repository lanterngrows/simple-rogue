///get_level_block(grid_x, grid_y)
/*
 * returns the image_index of the level grid at the given position
 * It's faster to just use objLevel.image_grid[# x, y]
 * so use that if you prefer.
 */
return objLevel.image_grid[# argument0, argument1];
