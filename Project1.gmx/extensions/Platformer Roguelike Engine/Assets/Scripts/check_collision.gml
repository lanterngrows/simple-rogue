///check_collision(x_offset, y_offset)
/*
 * Check if there's a collision at a given position
 * used the object's bounding box and the given x_offset & y_offset
 * 
 * x_offset - relative x position to check the collision
 * y_offset - relative y position to check the collision
 *
 * returns true if there's a collision, otherwise false.
 */
var xx = argument0;
var yy = argument1;
var t = 1/global.tile_size;

var x1 = floor((bbox_left+xx)*t);
var y1 = floor((bbox_top+yy)*t);
var x2 = floor((bbox_right+xx)*t);
var y2 = floor((bbox_bottom+yy)*t);

var grid = objLevel.level_grid;
var up_l = grid[# x1, y1];
var up_r = grid[# x2, y1];
var dn_l = grid[# x1, y2];
var dn_r = grid[# x2, y2];

var solid_collision = (block_solid(up_l) ||
                       block_solid(up_r) ||
                       block_solid(dn_l) ||
                       block_solid(dn_r));

///If we're in collision with a solid block
///return true straight away
if (solid_collision)
{
    return true;
}
///If not... check for semi-solid (fallthrough) platforms
else
{
    ///Only check for platforms if the "fallthrough" flag is false
    ///and our vertical speed is >= 0
    if (!fallthrough && vy >= 0)
    {
        var semi_collision = (block_semi_solid(up_l) ||
                              block_semi_solid(up_r) ||
                              block_semi_solid(dn_l) ||
                              block_semi_solid(dn_r));
        if (semi_collision)
        {
            y1 = floor((bbox_top+yy-1)*t);
            y2 = floor((bbox_bottom+yy-1)*t);
            ///Only collide with the fallthrough platform
            ///if the area above us is not colliding with the platform
            up_l = grid[# x1, y1];
            up_r = grid[# x2, y1];
            dn_l = grid[# x1, y2];
            dn_r = grid[# x2, y2];
            var above_collision = (block_semi_solid(up_l) ||
                                   block_semi_solid(up_r) ||
                                   block_semi_solid(dn_l) ||
                                   block_semi_solid(dn_r));
            if (!above_collision)
            {
                return true;
            }
        }
    }
}
                       
return false;
