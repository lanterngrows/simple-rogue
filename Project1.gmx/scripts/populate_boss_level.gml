///populate_level()
/*
 * After generating the level, you want to populate it with enemies & items, right?
 * Well that's what this script does.
 * Using global variables for the amounts of each enemy and item.
 */
if global.current_room == objBossLevel { exit; }
 
var px, py, i, obj, a, yy, obj, spr;
var s = global.tile_size;
var s2 = s/2;

/*
//Boxes
repeat(round_chance(global.level_boxes))
{
    obj = objBox;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Minecart
repeat(round_chance(global.level_minecarts))
{
    obj = objMinecart;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
*/

//Mushrooms
repeat(round_chance(global.level_mushrooms))
{
    obj = objMushroom;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    var a = random_spawn(obj, s2, s-yy, true, false);
    if (a != noone)
    with (a)
    {
        block_x = floor(x/s);
        block_y = floor(y/s)+1;
    }
}

