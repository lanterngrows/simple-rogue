///level_end()
/*
 * End the level and return to the menu
 * Destroy global.current_room, reset the level number and
 * go to roomMenu
 */
/*if (surface_exists(global.current_room.level_surface))
{
    surface_set_target(global.current_room.level_surface);
        draw_clear_alpha(0, 0);
    surface_reset_target();
}*/
with (global.current_room) instance_destroy();
global.level = 1;
room_goto(roomMenu);
