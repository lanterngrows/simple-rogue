///populate_level()
/*
 * After generating the level, you want to populate it with enemies & items, right?
 * Well that's what this script does.
 * Using global variables for the amounts of each enemy and item.
 */ 
var px, py, i, obj, a, yy, obj, spr;
var s = global.tile_size;
var s2 = s/2;
//Crabs
repeat(round_chance(global.level_crabs))
{
    obj = objCrab;
    spr = object_get_sprite(obj);
    yy = sprite_get_height(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Bats
repeat(round_chance(global.level_bats))
{
    obj = objBat;
    random_spawn(obj, s2, s2, false, true);
}
//Pots
repeat(round_chance(global.level_pots))
{
    obj = objPot;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Boxes
repeat(round_chance(global.level_boxes))
{
    obj = objBox;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Minecart
repeat(round_chance(global.level_minecarts))
{
    obj = objMinecart;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Gold
repeat(round_chance(global.level_gold))
{
    obj = choose(objGoldSmall, objGoldLarge, objGem, objGemSmall);
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Treasure chests
repeat(round_chance(global.level_chests))
{
    obj = objChest;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//SUPER Treasure chests
repeat(round_chance(global.level_big_chests))
{
    obj = objBigChest;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Spikes
repeat(round_chance(global.level_spikes))
{
    obj = objSpikes;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    var a = random_spawn(obj, s2, s-yy, true, false);
    if (a != noone)
    with (a)
    {
        block_x = floor(x/s);
        block_y = floor(y/s)+1;
    }
}
//Slimes
repeat(round_chance(global.level_slimes))
{
    obj = objSlime;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    random_spawn(obj, s2, s-yy, true, false);
}
//Campfires
repeat(round_chance(global.level_campfires))
{
    obj = objCampfire;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    var a = random_spawn(obj, s2, s-yy, true, false);
    if (a != noone)
    with (a)
    {
        block_x = floor(x/s);
        block_y = floor(y/s)+1;
    }
}
//Mushrooms
repeat(round_chance(global.level_mushrooms))
{
    obj = objMushroom;
    spr = object_get_sprite(obj);
    yy = sprite_get_bbox_bottom(spr)-sprite_get_yoffset(spr);
    var a = random_spawn(obj, s2, s-yy, true, false);
    if (a != noone)
    with (a)
    {
        block_x = floor(x/s);
        block_y = floor(y/s)+1;
    }
}

