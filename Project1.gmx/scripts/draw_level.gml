///draw_level(x1, y1, x2, y2)
/*
    draws the level's grid to the level surface
    so that we don't have to draw all of the blocks
    individually, and can just draw the surface
    instead! Much faster!
*/
var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;
var s = global.tile_size;

///Loop though each of the grid's cells and
///draw the block
surface_set_target(global.main_surface);
    draw_set_blend_mode(bm_subtract);
    draw_rectangle(x1*s, y1*s, x2*s-1, y2*s-1, false);
    draw_set_blend_mode(bm_normal);
    for (var xx = x1; xx < x2; xx++)
    for (var yy = y1; yy < y2; yy++)
    {
        var spr = get_level_block(xx, yy);
        if (!block_empty(spr))
        {
            var ind = get_level_block_index(xx, yy);
            draw_sprite(spr, ind, xx*s, yy*s);
        }
    }
surface_reset_target();

