///level_start(level_number, next_level)

with (global.current_room)
{
    /*if (surface_exists(level_surface))
    {
        surface_set_target(global.current_room.level_surface);
            draw_clear_alpha(0, 0);
        surface_reset_target();
    }*/
    instance_destroy();

}

if (instance_exists(global.current_room)) {
    with (global.current_room) instance_destroy();
}


global.level = argument0;
var next_level = argument1;
turn_on_player_knockback();

switch (next_level) {
    case EXPLORE_LEVEL:
        global.current_room = instance_create(0, 0, objLevel);
        global.current_room_alias = EXPLORE_LEVEL;
        break;
        
    case BOSS_LEVEL:
        turn_off_player_knockback();
        global.current_room = instance_create(0, 0, objBossLevel);
        global.current_room_alias = BOSS_LEVEL;
        break;
    
    /*    
    case TREASURY_LEVEL:
        global.current_room = instance_create(0, 0, objTreasuryLevel);
        global.current_room_alias = TREASURY_LEVEL;
        break;
    */
        
    case SHOP_LEVEL:
        global.current_room = instance_create(0, 0, objShopLevel);
        global.current_room_alias = SHOP_LEVEL;
        break;
}

room_goto(roomLevel);

