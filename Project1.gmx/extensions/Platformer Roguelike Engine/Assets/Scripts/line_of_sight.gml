///line_of_sight(x1, y1, x2, y2)
/*
 * Checks for solid blocks between (x1, y1) and (x2, y2)
 * x1, y1, x2, y2 - positions (in pixels, not blocks)
 * 
 * returns true if there are no blocks between the points,
 *         false otherwise.
 *
 * If you want a speed boost, increase the precision variable
 * but it should be fairly fast already...
 */
var s, x1, y1, x2, y2, xx, yy, dir, dis, i, result, precision;
precision = 0.5;
s = 1/global.tile_size;
x1 = floor(argument0*s);
y1 = floor(argument1*s);
x2 = floor(argument2*s);
y2 = floor(argument3*s);
dir = point_direction(x1, y1, x2, y2);
dis = point_distance(x1, y1, x2, y2);
result = true;
for (i = precision; i < dis; i+=precision)
{
    xx = floor(x1+lengthdir_x(i, dir));
    yy = floor(y1+lengthdir_y(i, dir));
    if (block_solid(objLevel.level_grid[# xx, yy]))
    {
        result = false;
        break;
    }
}
return result;

