///get_minimap_y(value)
var border_y = objLevel.border_height*global.tile_size*2;
var yy = argument0;
var map_y = (yy/(objLevel.pixel_height-border_y)*objMiniMap.map_height-objMiniMap.chunk_height);
return map_y;
