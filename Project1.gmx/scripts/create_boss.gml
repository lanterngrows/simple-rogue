///create_boss()
/*
    Creates a boss at the exit door
    OR if a boss already exists, move the boss
    to the exit door's position
*/
with (objDoorExit)
{
    if (instance_exists(global.boss))
    {
        var boss = global.boss
    }
    else
    {
        var boss = instance_create(0, 0, global.boss);
    }
    boss.x = x;
    boss.y = y-(global.tile_size*2.5);
    
    with (objBossHUD)
    {
        target = boss;
        boss_name = boss.boss_name;
    }
}
