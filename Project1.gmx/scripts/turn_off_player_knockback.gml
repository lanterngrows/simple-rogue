///turn_off_player_knockback

with objPlayer1 {    
    with melee_weapon {
        knockback = 0;
    }

    with ranged_weapon {
        knockback = 0;
    }
}

